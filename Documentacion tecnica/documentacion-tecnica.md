# Documentacion tecnica

## Tipos de documentacion


1. Guias de inicio
2. FAQ y videos tutoriales
3. Guias de evaluacion
4. Avisos de seguridad
5. Blogs
6. Troubleshooting
7. Manuales de uso

## Documentacion y UX

### Usabilidad

- Mapa de web
- Navegabilidad: indices, buscadores, miga de pan
- No contener paginas rotas

### Accesibilidad

- Separado por niveles de conocimientos y capacidades
- Adaptado a todos los dispositivos(responsive)
- Distintos modos de ejectura acciones(personas con capacidades distintas)
- Recursos visuales (imagenes, gifs)

### Consistente

- Mantener el mismo estilo
- Mismo diseño para paginas similares
- Usar misma terminologia y estilo

## Errores comunes

- Dificil de encontrar (esta alejado del producto)
- No tiene capturas de pantalla o tiene demasiadas
- Demasiada narrativa o solo hay videos
- Las instrucciones no estan separadas por pasos (no colocar un solo texto largo)
- Titulos pocos descriptivos o largos
- Documentacion desactualizada


# Sistema de control de versiones => Cadena de revisiones => Publicacion automatica

## Las 7 C de la escritura tecnica

- Clara
- Concreta
- Concisa
- Cohesiva
- Consistente
- Completa
- Coherente

## Medir los resultados

**Herramientas:**
- Google Analytics
- hotjar
- Soporte / Atencion al cliente

## KPIs para medir la UX

1. Satisfaccion de los usuarios
    - NPS: tener mas promotores que detractores
    - Visitas al sitio
    - Inbound links:  enlaces desde sitios externos
2. Usabilidad y ayuda
    - Encuentras, iconos de sentimientos
    - Keywords y terminos mas buscados
    - CTR, tasa de rebote, salidas
3. Calidad de la documentacion
    - Feedback en soporte
    - NPS
    - Casos de soporte resueltos.

## Metricas en Google Analytics


| Variables de control | Buena metrica | Metrica buena/media | Mala metrica |
| ------ | ------ | ------ | ------ |
| Tiempo de pagina | -2 min | 2.01-3.59 min | +4 min | 
| Tasa de rebote | -40% | 41-55% | +60% |
| Salidas | +50% | 30-50% | -30% |


## Mejorar la UX de la documentacion

1. Revisa los casos de soporte
2. Crear nueva doc, mejora la existente, borra la desactualizada
3. Test A/B (cambiar el diseño de la pagina, revisar la manera de presentar)
4. Medir resultados

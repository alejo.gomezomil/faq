# Kubernetes

## Minikube

### minikube not start
- minikube not start as root
- install conntrack
    - yum install conntrack

### $ The connection to the server localhost:8080 was refused - did you specify the right host or port?

Check availability
`docker ps | grep kube-apiserver`

Try this:

  ```
mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
### Forwardear un puerto
`kubectl port-forward svc/mongo 27017:27017`

# MINIKUBE FAQ

## Instalar dentro de una VM de VirtualBox (debian)

### Nota: Instalar docker antes


1. Instalar kubectl
`curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.9.0/bin/linux/amd64/kubectl`
2. Make kubectl executable and move it to system path:

```
sudo chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```
3. Instalar MINIKUBE

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.24.1/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
minikube version
```
4. Inicializar minikube

`sudo minikube start --vm-driver=none`


## Troubleshooting

### Error con crictl
```
X Saliendo por un error RUNTIME_ENABLE: Temporary Error: sudo crictl version: exit status 1
stdout:

stderr:
sudo: crictl: command not found
```
### Solucion

**Install crictl**

```
VERSION="v1.25.0"
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/$VERSION/crictl-$VERSION-linux-amd64.tar.gz
sudo tar zxvf crictl-$VERSION-linux-amd64.tar.gz -C /usr/local/bin
rm -f crictl-$VERSION-linux-amd64.tar.gz
```

### Error HOST_JUJU_LOCK_PERMISSION

`X Saliendo por un error HOST_JUJU_LOCK_PERMISSION: Failed to start host: boot lock: unable to open /tmp`

### Solucion

`rm /tmp/juju-*`

### ERROR sudocrictl: not found if installed to a directory outside of sudoers secure_path

`sudo crictl: not found if installed to a directory outside of sudoers secure_path`

### Solucion

**Changing the secure_path to /usr/local/bin**

```
[rohit@localhost root]$ sudo visudo
Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin

```
### ERROR "Error from server (NotFound): the server could not find the requested resource"

### Solution

Upgrade kubectl client version -> https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

### Error Error caching kubectl: failed to acquire lock “/root/.minikube/cache/linux/v1.22.2/kubectl.lock“:

### Solution
```
sudo rm -rf /tmp/juju-mk*
sudo rm -rf /tmp/minikube*
```


# Install kubeadm

_Guide to Centos 7_

1. Install necessary packages
2. Install docker
3. Configure docker - Permissions and storage driver
4. Install kubeadm and kubectl
5. Modify network and turn off memory swap
6. Configure firewall
7. Enable kubelet and pull images
8. kubeadm init and copy config to HOME

**Before all make a `yum update`**

## 1. Install necessary packages
- `yum -y install vim epel-release git htop curl wget nano lvm2 device-mapper-persistent-data yum-utils`

## 2. Install docker
- `yum-config-manager  --add-repo https://download.docker.com/linux/centos/docker-ce.repo`
- `yum install docker-ce`
- `systemctl enable docker`
- `systemctl start docker`
- `systemctl status docker`

## 3. Configure docker 
### Permisions 
- `sudo groupadd docker`
- `sudo usermod -aG docker $USER`
- `newgrp docker`
- Test with `docker run hello-world`

### Storage driver config
```
sudo tee /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF

sudo systemctl daemon-reload 
sudo systemctl restart docker
sudo systemctl enable docker
```

## 4. Install kubeadm and kubectl

### Add repo
```
sudo tee /etc/yum.repos.d/kubernetes.repo<<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

*ERROR: _https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64/repodata/repomd.xml: [Errno -1] repomd.xml signature could not be verified for kubernetes_  **change gpgcheck=1 to gpgcheck=0**

### Install kubeadm and kubectl

- `sudo yum -y install kubelet kubeadm kubectl --disableexcludes=kubernetes`

### Check installation
- `kubeadm  version`
- `kubectl version --client`

## 5. Disable SELinux, turn off memory swap and modify network

### SELinux
- `sudo setenforce 0`
- `sudo sed -i 's/^SELINUX=.*/SELINUX=permissive/g' /etc/selinux/config`

### Turn off swap
- 
```
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
```
- `sudo swapoff -a`

### Modify network
- `sudo modprobe overlay`
- `sudo modprobe br_netfilter`

```
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sudo sysctl --system
```

## 6. Configure firewall

### Master Server ports
```
sudo firewall-cmd --add-port={6443,2379-2380,10250,10251,10252,5473,179,5473}/tcp --permanent
sudo firewall-cmd --add-port={4789,8285,8472}/udp --permanent
sudo firewall-cmd --reload
```
### Worker Node ports
```
sudo firewall-cmd --add-port={10250,30000-32767,5473,179,5473}/tcp --permanent
sudo firewall-cmd --add-port={4789,8285,8472}/udp --permanent
sudo firewall-cmd --reload
```

## 7. Enable kubelet and pull images

- `sudo systemctl enable kubelet`
- `sudo kubeadm config images pull`

## 8. kubeadm init and copy config to HOME
- `sudo kubeadm init`
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## 9. Adding network plugin

**Error/Bug:** _Container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized_

`kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/2140ac876ef134e0ed5af15c65e414cf26827915/Documentation/kube-flannel.yml`

## 10. Set node with worker label
`kubectl label node <node> node-role.kubernetes.io/worker=worker`

## 11. Problem with Flannel. /run/flannel/subnet.evn not found

```
kubeadm init --control-plane-endpoint=whatever --node-name whatever --pod-network-cidr=10.244.0.0/16
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
restart all
systemctl stop kubelet
systemctl stop docker
iptables --flush
iptables -tnat --flush
systemctl start kubelet
systemctl start docker
```

## 12. apply vs create

The key difference between kubectl apply and create is that apply creates Kubernetes objects through a declarative syntax, while the create command is imperative.

The command set kubectl apply is used at a terminal's command-line window to create or modify Kubernetes resources defined in a manifest file.



# Product Owner

# SABER DECIR NO!!!

1. El PO recibe ideas de los interesados y las transforma en historias de usuario.
2. **Descarta historias.**
3. Las historias que quedan funcionan como un embudo, se van fragmentando en problemas mas pequeños.
4. El equipo tecnico decide cuantas historias puede entregar por semana/sprint.
5. El equipo tecnico le da un costo a las historias y los stackholders un valor. El PO se encarga de establecer la relacion y determinar si es factible. **VALUE = Knowledge value(?) + Customer Value ( =)[carita feliz])** 
6. Weekly para el punto 5.
7. 3 enfoques:
    * Build the right thing -> Lo que el PO quiere.
    * Build the  thing right -> Lo que el TEAM quiere.
    * Build it fast -> Lo que el SCRUM quiere.

    Entre los 3 tratan de establecer un foco correcto para el producto
8. **El PO debe gestionar expectativas de manera realista**
9. Se puede establecer un diagrama burn-up 
    * Cuantas historias entregamos en X tiempo.
    * Establecer historias con valor: Empieza con un grafico plano (mientras se testea el valor de las historias), prosigue una curva (cuando se logra determinar las historias con mas valor), y termina en una meseta (cuando ya no quedan historias de valor significativo por realizar) esperando el fin del proyecto u otra version.
10. La idea no es producir mas si no entregar mas produciendo menos.

**El PO define el scope del sprint.**

# El Manifesto Agil

1. Individuos e interacciones       sobre Procesos y herramientas
2. Software que funciona            sobre Documentacion exhaustiva
3. Colaboracion con el cliente      sobre Negociacion de contratos
4. Responder ante el cambio         sobre Seguimiento de un plan

## 1. Individuos e interacciones
Las herramientas y procesos deben adaptarse a la organizacion y no al revez.

## 2. Producto funcionando sobre documentacion exhaustiva

El manifesto agil reconoce que al fin y al cabo la unica medida real de avance de un proyecto es la entrega de valor. (Entrar un documento y no un producto funcionando no aporta valor al negocio.)

## 3. Colaboracion con el cliente sobre la negocicacion contratcual
Se suma al cliente al equipo. Se intenta evitar que sea un contrato que sea aplicado al principio y sin cambios. Se trabaja en un marco contratcual cambiante de alto nivel basado en los resultados logrados.

## 4. Respuesta ante el cambio sobre seguir un plan
Se evitar tener un plan fijo pre-establecido.


# Principios del manifesto Agil

1. Nuestra mayor prioridad es satisfacer al cliente mediante la entrega temprana y continua 
de productos con valor. 
2. Aceptamos que los requisitos cambien, incluso en etapas tardías del desarrollo. Los 
procesos Ágiles aprovechan el cambio para proporcionar ventaja competitiva al cliente. 
3. Entregamos producto funcionando frecuentemente, entre dos semanas y dos meses, con 
preferencia al periodo de tiempo más corto posible. 
4. Los responsables de negocio y los desarrolladores trabajamos juntos de forma cotidiana 
durante todo el proyecto. 
5. Los proyectos se desarrollan en torno a individuos motivados. Hay que darles el entorno y 
el apoyo que necesitan, y confiarles la ejecución del trabajo. 
6. El método más eficiente y efectivo de comunicar información al equipo de desarrollo y 
entre sus miembros es la conversación cara a cara. 
7. El producto funcionando es la medida principal de progreso. 
8. Los procesos Ágiles promueven el desarrollo sostenible. Los promotores, desarrolladores y usuarios debemos ser capaces de mantener un ritmo constante de forma indefinida. 
9. La atención continua a la excelencia técnica y al buen diseño mejora la Agilidad. 
10. La simplicidad, o el arte de maximizar la cantidad de trabajo no realizado, es esencial. 
11. Las mejores arquitecturas, requisitos y diseños emergen de equipos autoorganizados. 
12. A intervalos regulares el equipo reflexiona sobre cómo ser más efectivo para a continuacion ajustar y perfeccionar su comportamiento en consecuencia.

## Variables en los proyectos

1. En el metodo cascada el **alcance es una constante** y las variables tiempo y costo pueden ajustarse.
2. En el metodo agil **el costo y el tiempo son la constante** y el alcance puede ajustarse.

## Product Owner

Encargado de:
- **Incepcion**
- **Mapeo de historias de usuario**
- **Entrega de MVP**
- **Mantener un backlog refinado y actualizado**

### Responsabilidades

#### Gestion economica
Aprovechar recursos de la organizacion para maximizar la entrega de valor a nivel release y sprint

- Nivel release: Este punto es de fundamental importancia para definir los trade-off en alcance, fecha, presupuesto y calidad a medidad que se obtiene un avance.
- A nivel sprint: El PO es el encargado de asegurarse de un bueno retorno de inversion (ROI) al final de cada sprint. Atacando siempre lo mas valioso o prioritario.

#### Planificacion
El PO es parte vital en la planificacion. Clarifica dudas, lleva informacion o datos del producto y transmite las prioridades de la organizacion. De estos eventos debe llevarse la informacion a los interesados.

#### Refinamiento, priorizacion y mantenimiento del backlog
#### Definir criterio de aceptacion y verificar su cumplimiento
Establecer criterios facilita entender y preparar las pruebas que se realizaran. La existencia de estos criterios son mandatorios a la hora de planificar un item del product backlog en un sprint y al finalizar debe validad su cumplimiento.

#### Colaborar con equipo de colaboracion
La colaboracion con el equipo es fundamental y un buen PO debe tener comunicacion fluida con el equipo scrum.

#### Colaborar con los stackholders
Colaborar con los interesados internos y externos para entender sus necesidades.
Coordinar los trade-off y entraga de valor. Mantener transparencia y alinear expectativas.

### Caracteristicas

- Habilidades en el dominio 
    - Si tiene mas exp. en el dominio = facilidad toma de decisiones y movilidad
    - Si tiene menos exp. = mayor incertidumbre

- Habilidades de comuncacion y negociacion: Tener buena relacion con los stackholders y el equipo de elaboracion. Genera confianza, transparencia y concenso. Genera mayor cantidad de puntos de vista y experiencias.

- Poder de decision: Poder definir y tomar decisiones de acuerdo a lo mas prioritario. Poder destrabar conflictos de intereses y/o definiciones. Mantener balance entre el lado tecnico y el negocio para maximizar la entrega de valor.

- Responsabilidad y compromiso: Principal responsable de que los recursos se esten usando bien para la entrega de valor. Es el responsable de priorizar y refinar el backlog.

- Entusiasta y critico: Tener una vision de un producto que crea que benificia al usuario y al cliente y en donde vale la pena invertir. Ser critico para no pasarse de entusiasta.

- Informado e intuitivo: Constantemente procesando informacion y feedbacks mas que opiniones e ideas.

## Tipos de PO


| Product Onwer | Beneficios | Contras |
| ------ | ------ | ------ |
| Grande | Rapidez y consistencia en la toma de decisiones | Requiere una diversa variedad de habilidades y conocimientos. Potencialmente gran carga laboral. |
| Pequeño | Mayor foco y grado de especialización en las tareas |  Transferencias de conocimientos y objetivos. Potenciales retrasos y pérdida de conocimiento Inconsistencia en la toma de decisiones. |

 
## Escalamiento del rol de PO

### Escalando ciclo de vida del producto

1. Cuando recien empieza y esta como PoC, puede existir un solo rol, para ser mas efectiva la toma de decisiones que necesitan cambiar el producto como tal.
2. Cuando el producto sale al mercado, y el equipo ha crecido, es posible considerar a otra persona para escalar, ya que la toma de decisiones se vuelve mas complicada.

### Escalando por dueño y funcionalidades

Ej. Una app crece, y cada modulo de la app tiene su propio PO.
Ej. Una sucursal financiera crece y cada sector tiene su propio PO (prestamos, prestamos a pymes, prestamos hipotecarios)

### Escalando por variantes del producto.

Divir el producto, cuando el original ha crecido mucho. Esto genera un nuevo PO y un equipo de elaboracion nuevo.
Ej: Un ISP tiene una app para sus clientes con un canal de mensajaria propio. El producto ha crecido tanto que se separa de la idea original
Ej: Facebook y Messenger

## ¿Quien deberia ocupar el rol de PO segun el proyecto?

### Desarrollos internos
En este caso es deseable seleccionar una persona del grupo que se beneficiara con el desarrollo y conozca el dia a dia de la operacion.

### Desarrollos comerciales
Debe ser una persona de la empresa que haga de voz del cliente. Posiblemte algun contratista para suplir al cliente en el rol.

### Desarrollo tercerizado
Caso inverso. La empresa contratada asigna un PO que no delega la responsabilidad y es el encargado de interactuar y liberar bloqueantes.

### Desarrollo de componentes
Cuando se genera un componente que es parte de una solucion mas grande. Se integra con otras partes desarrolladas. Preferiblemente un perfil tecnico que puedan priorizar y definir este tipo de items.





# El backlog del producto y sus componentes (PBIs)
## Como expresar items en el product backlog

- **Caracteristicas y funciones**
    - Historias de usuario: Desde el punto de vista del usuario. COMO-QUIERO-PARA
    - Casos de uso
- **Errores o bugs:** Fallas detectadas en entregables terminados.
- **Trabajo tecnico:** Puede ser necesidades como implementar un parche del SO, algun update de alguna tecnologia. Pueden no ser pedidos por el PO, pero deben ser considerados para el mantenimiento y mejora del producto.
- **Adquisicion de conocimiento:** Este tipo de items no tiene valor real para el usuario pero sirve para obtener un feedback y poder definir nuevos items a traves del conocimiento adquirido. Deben ser de duracion limitada y un alcance concreto.

## Informacion necesaria de cada PBI

_No es un template para definir los items_

**Idea de los datos que debe tener un item del PB:**

- Descripcion
- Estimacion
- Orden de importancia

## Atributos que involucran a los PBI:

- **Valor:** Beneficion que otorga al usuario
- **Riesgo:** Dificultados o peligros. Ayuda a ver si se realiza o no
- **Costo:** Valor monetario que costara realizarlo
- **Recursos:** Skills, infra, licencias, etc
- **Conocimiento:** Necesidad de obtener informacion sobre la implementacion o sobre el comportamiento del usuario
- **Dependencias:** La necesidad de otros desarrollo que lo afectan.

## Que no es un PBI

**El backlog no es una lista de tareas sino de entregables del producto.**

# Estimar o No Estimar

### ¿Para que se estima?
Cada uno puede tener una respuesta distinta a esta pregunta. Desde un punto de vista agil nos preguntamos el objetivo, el valor y asi actuar en consecuencia
### ¿Cuando se estima?
- Al momento de evaluar posibles potenciales proyectos.
- Al iniciar un proyecto.
- Al inicia de una etapa.
- Diariamente.
### Costo de la estimacion
**Es un entregable interno del proyecto, no agrega valor al usuario.**
~~¿Cuando va a estar?~~  **No agregar presion al equipo**
Preguntas que se quiere responder:
- ¿Cual es la hipotesis que queremos validar?
- ¿Cual es el valor del usuario?
### Riesgo o Incertidumbre al momento de estimar
_Cono de incertidumbre_ (Bary Boehm)
**Una estimacion temprana tendra mucha incertidumbre. Una estimacion tardia tendra mas certeza.**

### Estimaciones agiles




# Roadmaps de Producto y plan de releases

## Roadmaps de producto

### Tipos de roadmap

- Basados en funcionalidades
- Basados en Objetivos

### Roadmap segun sus audiencias

#### Roadmap internos

1. Ejecutivo: Enfocar el roadmap en  el ROI, mejora de posicion en el mercado, estrategia, etc.
2. Equipo que participa en la construccion que necesitamos
3. Sector comercial encargado de vender el producto (evitar fechas fijas, comprometerse a cosas imposibles)

#### Roadmap externos

Roadmap expuesto al publico. No asumir fechas fijas.
Puede variar.

#### Template de roadmap

## Planificacion Agil de Releases

El plan de releases expresa como se espera alcanzar los objetivos con definiciones mas concretas, se incluye en el roadmap y tiene plazo en entre 3 y  meses.

### Delivery vs Discovery

La fase de discovery realiza una investigacion inicial de lo que quiere el usuario-
La fase de delivery realizar una entrega estable y escalable del producto.

En el metodo Agile, las fases de retro-alimentan entre ellas en un proceso continuo de aprendizaje y desarrollo.
La idea no es maximizar la implementacion de **ideas no validadas** si no acortar y reducir el costo entre la idea y la entrega de valor. Se valida cuando los usuarios reales lo usan.
Siempre se elige la hipotesis, riesgo o pregunta que mas miedo de y se identifica un experimento para aprender.

## Intro a las historias de usuario

_Narrar historias revela significado sin cometer el error de definirlo_ Hannah Arendt

_"El backlog debe describir items de valor para el usuario, no nuevas funcionalidades, temas a investigar, fixes"_ Mike Cohn

Historias de usuario en 3 C: 
- Card: Manera en la que se presenta una historia de usaurio
- Conversacion: Eran de tamaño reducido para no tener espacio para una descripcion y asi incitar a la conversacion.
- Confirmacion: Debe quedar validada de como se va a llevar a cabo.

**Estandar para escribir historia de usuario**

- **COMO** rol o usuario
- **QUIERO** objetivo o meta
- **PARA** razo o motivacion

**COMO** un cliente ecommerce **QUIERO** acceder facilmente los elementos en el carrito de compras **PARA** conocer y modificar mi seleccion de compra actual

## Mapeo de historias de usuario (user history mapping), un enfoque visual

### 1er paso

Conversar sobre el problema que queremos resolver

La idea es tener un backbone de la cual se obtiene una lista de activdades y va iterando.

### 2do paso
Se empieza a descomponer la columna sin preocuparse por priorizar si nomas por incorporar temas de conversacion.
Esto puede conducir a Epicas, historias de usuario de muy alto nivel.

### 3er paso
Empezar a priorizar, se puede cambiar el orden de la columna. Se puede aplicar un refinamiento, separando lo valioso o prioritario, asociandolo tambien a su costo o complejidad.

### 4to paso
Teniendo las prioridades x cada activdad, se maximiza el valor de cada una y se genera un nivel de items criticos. **Se genera un esquelento andante**. Esto se utiliza para validad hipotesis de la manera mas simple y entregando valor, esto genera un feedback interno de viabilidad.

### 5to paso
Si bien el esqueleto que se define puede ser un MVP, otras veces puede ser que no, en esas ocaciones es necesario agrandar el alcance para lograr  viabilidad y asi tener un MVP.

### 6to paso
Establecer proximox releases de acuerdo a las prioridades de cada instancia.

## Tecnicas de Priorizacion

### Modelo Kano

**Categorias descriptivas**
- **Obligatorias:** Son aquellas caracteristicas que los clientes asumen como implicitas del producto por lo cual no suelen explicarse como necesidad, pero su ausencia genera una gran insatisfaccion.
- **Unidimensional:** Caracteristicas que el cliente solicito explicitamente y tienen una relacion directamente proporcional con la calidad de su implementacion. **Por lo general cumpliendo las caracteristicas obligatorias y unidimensionales se obtiene un producto promedio del mercado facilmente reemplazable. **
- **Atractiva:** Caracteristicas que generan una gran satisfaccion en el cliente, pero que si no estan no generan descontento. Se podrian ver como diferenciadoras de otros productos.
- **Indiferente:** Estas caracteristicas no generan diferencia si existen o no. Por lo general sirven para beneficio de otro sector.
- **Reversa**: Caracteristicas que el cliente no quiere. Entre mas de estas existan mas insatisfecho va a estar el cliente.


## La Vision del producto

"Si no sabes lo que quieres, vas a terminar teniendo muchas cosas que no quieres." by Chuck Palahniuk

4 cualidades que debe tener una vision

1. Grande
2. Compartida
3. Inspiradora
4. Concisa

Preguntas que intenta responder la vision:

- Quienes van a comprar este producto? A quienes va dirigido?
- Cuales son las necesidades que el producto va a satisfacer?
- Cuales son los atributos para cumplir y definir el exito?
- Como se compara con productos existentes?
- Como y cuando vamos a ganar con este producto?
- Puede la organizacion desarrollar y entregar al cliente el prducto con la tecnologia y recursos que dispone?

Preguntas dificiles

- Por que no se hizo aun?
- Que otras opciones hay en el mercado?

**El PO debe ser testarudo con la vision pero flexible en los detalles**

### Relacion entre vision, estrategia y tactica

| Pregunta que responde | ... es parte de la ... |
| ------ | ------ |
| ¿Para que? | Vision |
| ¿Que? | Estrategia |
| ¿Como? | Tactica |

Vision -> Estrategia -> Tactica -> Backlog

Si hay muchos visionarios **consensuar una sola idea**, *de no ser posible preguntarse* **¿Estamos hablando de productos distintos?**

### Tablero de vision

Ayuda a responder, describir, testear, corregir y refinar la estrategia del producto
Contiene la siguiente informacion

1. Declaracion de la vision.
2. Grupo a quien se dirige.
3. Necesidad
4. Producto.
5. Valor.


## Cuadro de modelo de negocios

### Plan de negocio
Es una manera de declarar los objetivos y como planificamos cumplirlos.
Documento a largo plazo 3-5 años.
Muestra la sustentabilidad y estrategia.
**Puede terminar siendo un documento con mucha teoria e hipotesis no validadas concretamente.**

### Cuadro de negocios
1. Segmento de Clientes
    - Cantidad de segmentos
    - Composicion de los segmentos
2. Proposicion de valor
3. Canales
4. Relaciones con los clientes
5. Flujo de ingresos
6. Actividades claves
7. Recursos clave
8. Alianzas clave
9. Estructura de costo

### Lean Canvas

Adaptacion del cuadro de modelo de negocio donde se incorpora la cultura Lean Startup

Los items que se incorporan son los siquientes:

1. Problema
2. Solucion
3. Ventajas diferenciales
4. Principales Metricas

Se quitaron los siguientes items:

1. Recursos claves
2. Actividades claves
3. Relacion con los clientes
4.  Alianzas claves

## Propuesta de valor

*Si no logramos nosotros definir el * **"para que"** * comprar nuestro producto no podemos esperar que los clientes lo hagan.*

**Entender la competencia y el nicho que se pretende ocupar en el mercado** (esta idea no debe cambiar seguido)

### 1. Propuesta de Geoff Moore

    - Para - Clientes a quienes nos orientamos
    - Que no estan satisfechos con - Alternativa actual 
    - Nuestro producto es - Tipo de producto/servicio
    - Que provee - Principal capacidad de solucion
    - A diferencia de - La alternativa

### 2. Discurso del elevador

Evitar terminos tecnicos.
Posiblemente incluir metricas alto nivel
Informacion que se debe brindar en el discurso

- Pregunta: Es la manera en la que planteamos el problema que queremos solucionar
- Solucion: Como lo solucionamos con nuestro producto
- Mercado: Cual/es sectores son nuestros clientes esperados y/o que nicho del mercado esperamos ocupar
- Modelo de negocio: Como se  espera generar ganancia del producto

### 3. Made to Stick

**[Ejemplo de la industria probado] Para / en [nuevo dominio]**

## Mapeo de impacto (Impact Mapping)
Objetivo: Dar soporte a la alineacion estrategica de 'sponsor' del negocio, stackholders y equipos de construccion.

La idea es dividir las etapas del descubrimiento del producto (product discovery) en sus componentes. Cada nivel clarifica una pregunta especificar y evita realizar algunas antes de tiempo.

- ¿Para que?
- ¿Quien?
- ¿Como?
- ¿Que?






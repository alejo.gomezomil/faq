# FAQ

## No se puede asignar un argumento de tipo "string | null" al parámetro de tipo "string". El tipo 'null' no se puede asignar al tipo 'string'.ts(2345)

`JSON.parse(localstorage.getItem('cart')!)`

## Get from localstorage

Donde scope.products es la variable del localstorage, memberdata es una variable nueva donde se va almancera lo de scope.products.

```
$scope.listdata= localStorage.getItem("memberdata");

if($scope.listdata){
    $scope.listdata = JSON.parse($scope.listdata)

}else {
    localStorage.setItem("memberdata", JSON.stringify($scope.products));
}
```

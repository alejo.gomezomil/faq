# MySQL FAQ

## Problem to connect remote (its wrong, insecure but works)

Edit this en my.cnf

```
[mysqld]
bind-address = ::
```

## SQLSTATE[HY000]: General error: 1298 Unknown or incorrect time zone: 'UTC' windows

`shell> mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql`

`shell> mysql_tzinfo_to_sql tz_file tz_name | mysql -u root mysql`

`shell> mysql_tzinfo_to_sql --leap tz_file | mysql -u root mysql`

## MariaDB won't start on Ubuntu: Can't lock aria control file

Remote .aria file

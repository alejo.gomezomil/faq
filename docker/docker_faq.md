# DOCKER
## Instalar docker

```
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

sudo apt update

apt-cache policy docker-ce

sudo apt install docker-ce
```

## Instalar docker-compose

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version
```

## Borrar volúmenes sin utilizar

`docker volume ls -f dangling=true -q | xargs docker volume rm`

## Borrar imágenes sin utilizar
`docker images -f dangling=true -q | xargs docker rmi`


`Borrar todo lo que este parado`

```
docker system prune
You’ll be prompt you to confirm the operation:
WARNING! This will remove:
        - all stopped containers
        - all networks not used by at least one container
        - all dangling images
        - all build cache
Are you sure you want to continue? [y/N]
```


## Actualizar contenedor
```
docker-compose up --force-recreate --build -d
docker image prune -f
```

## Archivo /etc/systemd/system/docker.service.d

### Cambiar el dns server para los contenedores desde la conf
```
[Service]
ExecStart=
ExecStart=/usr/bin/docker daemon --dns 192.168.1.1 -H fd://
```


### Habilitar la API de forma remota de docker
```
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock
```


## Copiar desde contenedor a host
`docker cp <containerId>:/file/path/within/container /host/path/target`

## Deploy en Docker Swarm

`docker stack deploy -c phpmyadmin.yml phpmyadmin`

## Ver stats
docker top <container>
docker stats

## BACKUP y RESTORE EN DOCKER
### Backup

`docker exec <CONTAINER> /usr/bin/mysqldump -u root — password=root <DATABASE> > backup.sql`

### Restore
`cat backup.sql | docker exec -i <CONTAINER> /usr/bin/mysql -u root --password=root <DATABASE>`

### docker stuck in NEW state | level=error msg="Could not parse VIP address  while releasing"

Reiniciar docker
`service docker restart`

### Reiniciar un solo servicio del stack
`docker-compose up -d --no-deps name_of_your_container`



## Agregar repositorio remoto a nuestro repositorio local
`git remote add origin _<remote repository URL>_`

## Guardando cambios

| Comando | Descripcion | Operadores |
| ------ | ------ | ------ |
| `git stash` | El comando git stash agarra los cambios sin confirmar (tanto los que están preparados como los que no), los guarda aparte para usarlos más adelante y, acto seguido, los deshace en el código en el que estás trabajando | pop: Vuelve a colocar los archivos. Vacia el stash|
| | | apply:  Vuelve a colocar los archivos. Queda en el stash.|

## Debugeando

| Comando | Descripcion | Operadores |
| ------ | ------ | ------ |
| `git blame <archivo>` | Muestra el historial de commits de el archivo, con autor incluido. | |
| `git grep "text"` | Muestra los archivos que contienen la palabra. **Case sensitive** | -n: Muestra la linea |
|  |  |  -c: Muestra la cantidad de veces que se repita la linea conflictiva  |
| `git log -S "text"` | Busca un log de cuando se introdujo/estuvo el termino | |
| `git bisect <command>` | Use binary search to find the commit that introduced a bug. Revisa commit por commit. Los marca como good o bad | start: Empieza un biseccionamiento|
| | | bad: Si lo dejo vacio, implica que va a revisar todos los commits hasta **HEAD**|
| | | good <commit>: Desde donde empieza a revistar. Incluir ID del commit o el tag del ultimo _stable_ |
| | | reset: Cuando se termina de usar, se usa el operador reset para terminar el bisect |
| | | _*Usar primero bad y despues good. Usar star para empezar y reset para finalizar_ |
| | | _*Ir paso por paso compilando el codigo y probando._ |
| `git reflog` | . El reflog cubre todas las acciones recientes como cambios de branch, resets, commits, etc. _*Parecido al git log, pero muestra otras cosas, como cuando se clono un repo, etc_ | |

## Deshaciendo cambios

| Comando | Descripcion |  Operadores |
| ------ | ------ | ------ |
| `git rebase -i <commit>` | Ordena, simplifica, junta y edita nuestros commits ||
| `git revert <commit>` | _*Usar si los cambios se subieron al remoto._ Revierte uno o varios commits. De hecho no los revierte, si no que crea un nuevo commit evitando los cambios generados en el commit conflictivo.||
| `git reset [tipo] <commit>` | _*Usar si los cambios no fueron subidos al remoto._  | Soft: El reset soft es aquel en donde los cambios que hemos intentado resetear aún se mantienen visibles pero ya no forman parte de ningún commit en nuestro branch. Estos cambios se van a ver en nuestros archivos y sobre todo se van a ver reflejados en el Stage Area cuando ejecutemos un git status y veamos que esos cambios que intentamos deshacer forman parte del próximo commit, en el caso en que hagamos uno en este mismo momento |
| | | Mixed: El reset mixed es aquel en donde los cambios también se siguen manteniendo visibles en nuestros archivos, es decir que no fueron totalmente deshechos pero si consultamos el status de nuestro repositorio podríamos observar que los mismos no forman parte del Stage Area, es decir que se van a ver como si se hubieran hecho recién pero como si todavía no se hubiera corrido el comando git add |
| | | Hard: El reset va a comenzar por lo mismo que los demás, corriendo nuestro HEAD al commit establecido pero todos los cambios que se hayan hecho durante ese tiempo no solo no van a formar parte del Stage Area sino que no vamos a verlos tampoco en nuestros archivos actuales, es decir nuestro Working Area. |
| `git commit --amend`| Modificación del último commit. _*EVITAR USAR EN COMMITS PUBLICOS O EN REMOTOS_ |  |
| `git checkout --` | Esta es la mas facil. Vuelve a la rama donde apunta el HEAD. Todos los cambios no se ven reflejados.| |

## Restaurando el trabajo

### Ejemplo: Se uso un git reset --hard HEAD~2

1.  Con el comando `git reflog` podemos ver el estado pasado de HEAD. (_*No se usa un hash de un commit, si no al estado del HEAD en el pasado_)
2. `git checkout <HEAD donde queremos volver>`
3. Tengamos en cuenta que vamos a estar parados en un commit que aún no fue borrado por nuestro repositorio local pero el cual no pertenece a ninguna parte de nuestro árbol real de commits, con lo cual nos sale el mensaje de detached HEAD y en nuestro log no vamos a ver ningún branch.
4. Para solucionar esto crear una nueva rama. `git checkout -b restauracion`





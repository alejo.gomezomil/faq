# Wordpress

## Reset password
The Short way through MySQL This methods implements the direct SQL query execution to update the wp_users table value to reset our hacked WordPress password. 
1. Follow the first 3 steps of the above method. 
2. Go to SQL or MySQL section in your PHPMyAdmin. 
3. Now, enter the below given query:
 
`UPDATE `wp_users` SET `user_pass`= MD5('yourpassword') WHERE `user_login`='yourusername';` 
Put your new password in place of yourpassword and replace yourusername with your WordPress username. Click “Go” or similar option at the bottom to execute the query.

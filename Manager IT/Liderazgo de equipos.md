**Saltar de grupo a equipo -> Definir objetivos comunes.**

## Definir objetivos de manera SMART

S - Specific
M - Medible
A - Alcanzable
R - Realista
T - Temporal

## Ventajas de trabajar en equipo

1. Mejora la productividad. 
2. Disminuye la rotación de las personas.
3. Aumenta el bienestar laboral.
4. Favorece el desarrollo personal.

## Modelo de Bruce Tuckman para la gestión de equipos.

1. Formacion - Objetivos, Mision y Vision
2. Turbulencia - Aprender a hacer
3. Normalizacion - Habitos adquiridos. Eficiente.
4. Desempeño - Maestria. Eficacia

## Fase 1:  Formacion del equipo

### Objetivos
1. Definir el propósito, el sentido y la esencia del equipo.
2. Que el líder y el equipo se conozcan.
3. Establecer el modelo de gobierno del equipo.
4. Otorgar al equipo las herramientas necesarias para funcionar.

### Actividades
1. Transmitir el propósito del equipo. 
2. Definir de Roles y Responsabilidades.
3. Establecer las reglas de funcionamiento del equipo. 

### Competencias claves del lider
- Definir la misión, visión y valores.
- Generar compromiso y responsabilidad.

## Fase 2: Turbulencia

### Objetivos
- Capacitación de los miembros del equipo.
- Garantizar el funcionamiento definido del equipo.

### Actividades
- Aprender a hacer lo que se ha acordado que se va a hacer.
- Desaprender hábitos adquiridos que no están alineados con la misión, visión y valores del equipo.
- Facilitar el aporte del equipo.
- Realizar refuerzo positivo y corregir comportamientos negativos.

### Competencias claves del lider
- Facilitar el Aprendizaje
- Gestión emocional para el cambio
- Dar Feedback constructivo

## Fase 3: Normalizacion

### Objetivos
- Mejorar la productividad.
- Aumentar autonomia del equipo.

### Actividades
- Retar al equipo
- Fomentar canales de comunicacion

### Competencias claves del lider
- Construir, alimentar y reconstruir la confianza

## Fase 4: Desempeño

### Objetivos
- Mantener el bienestar del equipo.
- La mejora continua, para que continúen aprendiendo y fortaleciéndose.

### Actividades
- Estimular en sus compañeros la participación creativa, innovar y buscar soluciones adecuadas y oportunas.
- Dar ejemplo. Un buen líder inspira con su ejemplo.
- Alentar, empujar, animar. El líder debe crear empatía y fomentar energía positiva en sus integrantes.

### Competencias claves del lider
- Impulsar la motivacion

## Acompañamiento por parte del lider

- Orientadas a la tarea
- Orientadas al afecto

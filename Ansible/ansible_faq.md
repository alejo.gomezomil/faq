# Asnbible FAQ

## List version with yum list

```
- name: yum_command 
  yum:
    list=installed
  register: yum_packages
```


```
- debug: var=item
  with_items: "{{yum_packages|json_query(jsonquery)}}"
  vars:
    jsonquery: "results[?name=='tar']"
```
Where _tar_ is the name of the package to search

This to get only the version
```
- debug: var=item
  with_items: "{{yum_packages|json_query(jsonquery)}}"
  vars:
    jsonquery: "results[?name=='tar'].version"
```

## Insert usign mysql_query with an iteration over items

```
  - name: Insert packages on packages table
        community.mysql.mysql_query:
          login_host: <IP>
          login_user: <database user>
          login_password: <password>
          login_db: <database>
          query:
            - INSERT IGNORE INTO <table> (attribute1, attribute2, attribute3, attribute4, attribute5) VALUES (%s,%s,%s,%s,%s)
          positional_args:
            - "{{ item }}"
            - "{{ web_v1 }}"
            - "{{ web_v2 }}"
            - "{{ web_v3 }}"
        with_items: "{{ version }}"

```



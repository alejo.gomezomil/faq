# Ansible to docker Centos 8

- [ ] Create a user ansible-user with sudo permisions.
- [ ] Verify the permisions in the remote machine and master node.
- [ ] Verify the configuration file of ssh. **PublickeyAuthentication**
- [ ] Verify the configuration file of ssh. **AuthorizedKeysFile**

- Add `ansible-user ALL=(ALL) NOPASSWD: ALL` at the end of sudoers file. This is necessary for this user to run commands without entering passwords.

## Verify permisions in the remote machine
- `chmod go-w /home/ansible-user`
- `chmod 700 /home/ansible-user/.ssh`
- `chmod 600 /home/ansible-user/.ssh/authorized_keys`

## Remove podman and buildah
These packages, podman and buildah, have conflicts with docker-ce.
I don't figure out how fix this, so the unique solution that I discover was uninstall these packages.
I set this instructions in the playbook.

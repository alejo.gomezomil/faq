# FAQ Alertmanager

### Los _silences_ no persisten

Los permisos para escribir en la carpeta /data donde se guardan los _silences_ no sirven

```
mkdir /alertmanager/data
chown 65534:65534  /alertmanager/data
```

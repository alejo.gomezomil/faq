# FAQ


## Problema
`err="open /prometheus/queries.active: permission denied"`
## Solucion
Agregar root como user
```
# docker-compose.yml

 services:
  prometheus:
    image: prom/prometheus
    user: root
```

## Enable docker metrics

Set the configuration to enable the new flag metrics in docker.
```
echo '{ "metrics-addr" : "127.0.0.1:9323", "experimental" : true }' > /etc/docker/daemon.json
systemctl restart docker
```

# Weave
## Install Weave net

Weave act like a network manager that handle the communication between the container between the hosts.
The container never expose the ports and even this between container can communicate them.

```
sudo curl -L git.io/weave -o /usr/local/bin/weave
sudo chmod +x /usr/local/bin/weave
weave launch
```

[Weave install](https://www.weave.works/docs/net/latest/install/installing-weave/)

## Weave scope
Weave Scope automatically detects processes, containers, hosts. No kernel modules, no agents, no special libraries, no coding. Seamless integration with Docker, Kubernetes, DCOS and AWS ECS.

### Install on local
```
sudo wget -O /usr/local/bin/scope \
https://github.com/weaveworks/scope/releases/download/latest_release/scope
sudo chmod a+x /usr/local/bin/scope
sudo scope launch
```
### Run
`scope launch`

[Weave scope install](https://www.weave.works/docs/scope/latest/installing/)

## Launch weave scope on k8s

### Deploy pods with weave scope
`curl -L https://cloud.weave.works/launch/k8s/weavescope.yaml`

_The configuration starts a Replication Controller and Service. It also deploys a DaemonSet. A DaemonSet automatically deploys Pods onto new hosts are deployed into the cluster. The result is you can visualise your entire network without as it changes without having to manage the Scope deployment or deploying it onto these new hosts._

### Deploy task
`kubectl create -f 'https://cloud.weave.works/launch/k8s/weavescope.yaml'`

_To deploy Scope use the yaml with kubectl. It currently requires to set the validate to false; this will be fixed in future versions of Kubernetes._

Lists pods
`kubect get pods -n weave`

### Expose the service with a external IP

- `pod=$(kubectl get pod -n weave --selector=name=weave-scope-app -o jsonpath={.items..metadata.name})`
- `kubectl expose pod $pod -n weave --external-ip="172.17.0.65" --port=4040 --target-port=4040`

_By default, once deployed it will only be accessible from inside the cluster. You need to create a Service which exposes the port. In the command below we also expose the Service to the outside world via the external-ip parameter. Exposing the service to on a public IP is not recommended. Instead, it should require a VPN connection to access._

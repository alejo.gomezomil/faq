# FAQ ELK v7

## ERRORES

### Problema

`Elasticsearch: Max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]`

### Solucion

`sysctl -w vm.max_map_count=262144`


### No se pueden cargar los dashboards (son muchos)

```
PUT _cluster/settings
{
  "transient": {
    "search.allow_expensive_queries": "true"
  }
}
```


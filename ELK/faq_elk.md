### ERROR: Max docs fields 
```
PUT .apm-agent-configuration/_settings
{
  "index.max_docvalue_fields_search": 200
}
```
_200 or other value_

### [WARN ][o.e.c.r.a.DiskThresholdMonitor] [jSjxvbp] flood stage disk watermark [95%] exceeded on


### Exiting: could not start the HTTP server for the API: listen tcp 172.22.111.40:5066: bind: cannot assign requested address

_ESTE CAMBIO SE HACE EN EL ARCHIVO CONFIG DEL BEAT o APM-SERVER_

*Fixed this by changing http.host: 127.0.0.1 to <container-name> (name of the container) and then I was able to curl from the host machine.*

# ERRORES CON APM QUEUE FULL

El error "queue is full, 503" en el APM (Application Performance Monitoring) server indica que la cola de eventos en memoria ha alcanzado su capacidad máxima, lo que impide la recepción de nuevos eventos. Para resolver este problema, puedes ajustar el tamaño de la cola de eventos en memoria configurando el parámetro `queue.mem.events`.

Para calcular un valor adecuado para `queue.mem.events`, debes considerar los siguientes factores:

1. **Carga esperada del sistema**: Cuántos eventos esperas recibir por segundo.
2. **Capacidad de procesamiento**: Cuántos eventos puede procesar tu APM server por segundo.
3. **Memoria disponible**: Cuánta memoria tienes disponible para asignar a la cola de eventos.

### Pasos para calcular `queue.mem.events`

1. **Estimación de la tasa de eventos**:
   - Determina el número de eventos que tu APM server recibe por segundo (EPS). Este dato puede provenir de la monitorización actual de tu APM.

2. **Capacidad de procesamiento**:
   - Estima cuántos eventos puede procesar tu servidor APM por segundo. Esto puede depender de factores como el hardware, la configuración del servidor y las optimizaciones realizadas.

3. **Memoria disponible**:
   - Determina cuánta memoria tienes disponible para el APM server. La cola de eventos en memoria debe caber dentro de esta capacidad sin causar problemas de memoria.

### Fórmula

Una forma simplificada de calcular `queue.mem.events` es:

\[ \text{queue.mem.events} = \text{EPS} \times \text{Tiempo\_en\_cola\_permitido} \]

Donde:
- **EPS** es el número de eventos por segundo que esperas recibir.
- **Tiempo\_en\_cola\_permitido** es el tiempo máximo que estás dispuesto a permitir que los eventos se queden en la cola antes de ser procesados (en segundos).

### Ejemplo

Supongamos:
- Tu APM server recibe 1000 eventos por segundo (EPS = 1000).
- Quieres que los eventos no permanezcan más de 10 segundos en la cola (Tiempo\_en\_cola\_permitido = 10).

Entonces:

\[ \text{queue.mem.events} = 1000 \times 10 = 10000 \]

En este caso, deberías configurar `queue.mem.events` a 10000.

### Configuración

Una vez que hayas calculado el valor adecuado, puedes configurarlo en el archivo de configuración de tu APM server (`apm-server.yml`):

```yaml
queue.mem:
  events: 10000
```

### Monitorización y Ajuste

Es importante monitorizar el rendimiento del APM server después de realizar estos ajustes y estar preparado para realizar modificaciones adicionales según sea necesario. Puedes usar herramientas de monitorización para observar métricas clave como el uso de memoria y la tasa de eventos procesados.

Si tienes más detalles específicos sobre tu entorno, como la tasa exacta de eventos o la memoria disponible, puedo ayudarte a realizar un cálculo más preciso.
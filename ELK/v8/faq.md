# Uninstall ELK from Ubuntu

```
sudo apt-get remove --purge elasticsearch
dpkg --purge elasticsearch
```
or
```
apt-get --purge autoremove elasticsearch
```

```
sudo rm -rf /etc/elasticsearch
sudo rm -rf /var/lib/elasticsearch
```